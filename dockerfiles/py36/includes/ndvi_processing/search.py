import requests
import urllib3
import urllib
from urllib3.exceptions import InsecureRequestWarning


# Return a search result with a pattern and a config in parameters
def search_by_external_id(external_id, config):
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    url = config.url
    cmd = url + "/api/v1/services/search?&f=identification.externalId:like:" + external_id
    r = requests.get(cmd, verify=False)
    return r
