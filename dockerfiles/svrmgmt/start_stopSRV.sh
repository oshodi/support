#!/bin/bash
#echo "Received params 1:$1 2:$2 3:$3 4:$4 5:$5"
if [ $# -ne 5 ] ; then
  echo "Usage [program] {START/STOP/STAT} {DOMAIN} {USER} {PASSWORD} {SERVER}"
else 
  export OS_AUTH_URL="https://iam.eu-west-0.prod-cloud-ocb.orange-business.com/v3"
  export OS_PROJECT_NAME=eu-west-0
  export OS_USERNAME=$3
  export OS_IDENTITY_API_VERSION="3"
  export OS_IMAGE_API_VERSION="2"
  export OS_NETWORK_API_VERSION="2"
  export OS_REGION_NAME="eu-west-0"
  export OS_USER_DOMAIN_NAME=$2
  export OS_PASSWORD=$4
  if [ "$1" == "START" ] ; then
    openstack server start ${5}
  fi
  if [ "$1" == "STOP" ] ; then
    openstack server stop ${5}
  fi
  if [ "$1" == "STAT" ] ; then
    openstack server list
  fi
fi
