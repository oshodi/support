#!/bin/bash
# Create key pair for Terraform VMs creation on a domain
# usage ./keypairCreate.sh <DOMAIN_NAME>

# Domain name
DOMAIN_NAME=$1

# Working directory
CWD="$(pwd)"

# Key pair working folder
KEYPAIR_ROOT_FOLDER="keypair"
# Key pair path
KEYPAIR_DOMAIN_PATH=${CWD}/${KEYPAIR_ROOT_FOLDER}/${DOMAIN_NAME}

# Default comment
KEYPAIR_COMMENT="OrangePSTeam"

# Check argument
if [ -z ${DOMAIN_NAME} ]; then
   echo "[${DOMAIN_NAME}] Domain name parameter is mandatory"
   echo "[${DOMAIN_NAME}] usage ./keypairCreate.sh <DOMAIN_NAME>"
fi

mkdir -p ${KEYPAIR_DOMAIN_PATH}
if [ ! -f ${KEYPAIR_DOMAIN_PATH}/id_rsa_${DOMAIN_NAME} ]; then
   ssh-keygen -t rsa -b 4096 -C "OrangePSTeam" -f ${KEYPAIR_DOMAIN_PATH}/id_rsa_${DOMAIN_NAME} -q -P ""
else
   echo "[${DOMAIN_NAME}] Key exists already"
fi

