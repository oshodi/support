variable "instance_name" {
  description = "Name of the ECS instance and the associated volume"
}

variable "instance_count" {
  description = "Number of instances to launch"
  default     = 1
}

variable "attach_eip" {
  description = "Whether or not attache elastic IP (public IP)"
  default     = false
}

variable "ext_net_name" {
  description = "External network name (do not change)"
  default     = "admin_external_net"
}

variable "image_id" {
  description = "ID of Image to use for the instance"
}

variable "flavor_name" {
  description = "The flavor type of instance to start"
}

variable "network_id" {
  description = "The network ID to launch in"
}

variable "subnet_id" {
  description = "The subnet ID to launch in"
}

variable "security_groups" {
  description = "A list of security group IDs to associate with"
  type        = "list"
}

variable "sysvol_type" {
  description = "The type of the system volume: SATA for standard I/O or SSD for high I/O"
  default     = "SATA"
}

variable "sysvol_size" {
  description = "The size of the system volume in GB"
  default     = "40"
}

variable "key_name" {
  description = "The key pair name"
}

variable "availability_zone" {
  description = "The availability zone to launch where"
}

variable "metadata" {
  description = "A mapping of metadata to assign to the resource"
  default     = {}
}

variable "user_data" {
  description = "The user data to provide when launching the instance"
  default     = ""
}
