variable "name" {
  description = "The security group name"
}

variable "description" {
  description = "The security group description"
  default     = "Security Group managed by Terraform"
}

variable "create_self_ingress_rule" {
  description = "Wheter or not create default self ingress rules (allow all protocols from this group)"
  default     = true
}

variable "delete_default_egress_rules" {
  description = "Wheter or not create default egress rules (allow all protocols to any destination)"
  default     = false
}

variable "ingress_with_source_security_group_id" {
  description = "List of ingress rules to create where a security group is remote"
  type        = "list"
  default     = []
}

variable "ingress_with_source_cidr" {
  description = "List of ingress rules to create where a CIDR is remote"
  type        = "list"
  default     = []
}

variable "egress_with_source_security_group_id" {
  description = "List of egress rules to create where a security group is remote"
  type        = "list"
  default     = []
}

variable "egress_with_source_cidr" {
  description = "List of egress rules to create where a CIDR is remote"
  type        = "list"
  default     = []
}
