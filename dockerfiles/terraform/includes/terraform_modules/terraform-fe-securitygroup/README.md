# Flexible Engine Security Group Terraform Module

Terraform module which creates security groups and associated rules resource on Flexible Engine

## Usage

```hcl
module "ecs_cluster_sg" {
  source = "modules/terraform-fe-securitygroup"

  name        = "ecs_cluster"
  description = "Security group for ecs_cluster instances"
  
  ingress_with_source_security_group_id = [
    {
      from_port                = 22
      to_port                  = 22
      protocol                 = "tcp"
      ethertype                = "IPv4"
      source_security_group_id = "security-group-id"
    }
  ]
  ingress_with_source_cidr = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      ethertype   = "IPv4"
      source_cidr = "0.0.0.0/0"
    },
    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      ethertype   = "IPv4"
      source_cidr = "0.0.0.0/0"
    }
  ]
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| create_self_ingress_rule | Wheter or not create default self ingress rules (allow all protocols from this group) | string | `true` | no |
| delete_default_egress_rules | Wheter or not create default egress rules (allow all protocols to any destination) | string | `false` | no |
| description | The security group description | string | `Security Group managed by Terraform` | no |
| egress_with_source_cidr | List of egress rules to create where a CIDR is remote | list | `<list>` | no |
| egress_with_source_security_group_id | List of egress rules to create where a security group is remote | list | `<list>` | no |
| ingress_with_source_cidr | List of ingress rules to create where a CIDR is remote | list | `<list>` | no |
| ingress_with_source_security_group_id | List of ingress rules to create where a security group is remote | list | `<list>` | no |
| name | The security group name | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| security_group_id | The ID of the security group |
| security_group_name | The name of the security group |
