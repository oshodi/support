# Ubuntu 18.04
FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
#RUN apt-get upgrade -y

# install system tools
RUN apt-get install -y sudo wget

#install python 2.7 + pip + pillow
RUN apt-get install -y python2.7 python-pip python-pillow

RUN apt-get update

# install GDAL
RUN apt-get install -y python-gdal gdal-bin gdal-contrib libgdal-dev dans-gdal-scripts

# install jp2 image tools
RUN apt-get install -y libopenjp2-7 libopenjp2-tools

# Install zip utility
RUN apt-get install -y p7zip-full

# install convenient text editor
RUN apt-get install -y nano

# install boto3 for py27
RUN apt-get install -y python-boto3

# install eodag
RUN apt-get install -qy python-tk
RUN pip install eodag matplotlib ipyleaflet ipywidgets

# Install glob module
RUN pip install glob2

# install tools required by JRC
# Adding neurodebian repo
RUN wget -O- http://neuro.debian.net/lists/bionic.de-m.libre | sudo tee /etc/apt/sources.list.d/neurodebian.sources.list
RUN apt-key adv --recv-keys --keyserver hkp://pool.sks-keyservers.net:80 0xA5D32F012649A5A9
RUN apt-get update

#install dependencies
RUN apt-get install -y python-numpy python-scipy python-matplotlib ipython python-pandas python-sympy python-nose

#install tensorflow and scikit-learn
RUN pip install tensorflow scikit-learn

# setup eodag providers
RUN mkdir /etc/eodag
ADD ./includes/providers.yml /etc/eodag/providers.yml
ADD ./includes/eodagconf.yml /etc/eodag/eodagconf.yml
RUN mv /usr/local/lib/python2.7/dist-packages/eodag/resources/providers.yml /usr/local/lib/python2.7/dist-packages/eodag/resources/providers.yml.orig
RUN ln -s /etc/eodag/providers.yml /usr/local/lib/python2.7/dist-packages/eodag/resources/providers.yml

#Install tuto source code
RUN mkdir -p /sources
RUN ln -s /etc/eodag/eodagconf.yml /sources/eodagconf.yml

# copy and run bootstrap
ADD ./includes/bootstrap.sh /root/bootstrap.sh
RUN chmod +x /root/bootstrap.sh
RUN /root/bootstrap.sh

# Add sobloo logo at startup
ADD ./includes/motd /etc/motd
RUN echo '[ ! -z "$TERM" -a -r /etc/motd ] && cat /etc/issue && cat /etc/motd' >> /etc/bash.bashrc

# run cmdline
CMD [/bin/bash]

