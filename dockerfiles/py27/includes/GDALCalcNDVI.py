#! /usr/bin/env python

#######################################
# GDALCalcNDVI.py
#
# A script using the GDAL Library to
# create a new image contains the NDVI
# of the original image
#
# Author: <YOUR NAME>
# Email: <YOUR EMAIL>
# Date: DD/MM/YYYY
# Version: 1.0
#######################################

# Import required libraries from python
import sys, os, struct, glob
# Import gdal
import osgeo.gdal as gdal


# Define the class GDALCalcNDVI
class GDALCalcNDVI (object):

    # A function to create the output image
    def createOutputImage(self, outFilename, inDataset):
        # Define the image driver to be used 
        # This defines the output file format (e.g., GeoTiff)
        driver = gdal.GetDriverByName( "GTiff" )
      
        geoTransform = inDataset.GetGeoTransform()
        geoProjection = inDataset.GetProjection()
        # Create an output file of the same size as the inputted 
        # image but with only 1 output image band.
        newDataset = driver.Create(outFilename, inDataset.RasterXSize, \
                     inDataset.RasterYSize, 1, gdal.GDT_Float32)
        # Define the spatial information for the new image.
        newDataset.SetGeoTransform(geoTransform)
        newDataset.SetProjection(geoProjection)
        return newDataset
        
    # The function which loops through the input image and
    # calculates the output NDVI value to be outputted.    
    def calcNDVI(self, fileRed, fileNir, outFilePath):
        driver = gdal.GetDriverByName("")
        # Open the inputted dataset
        dataset = gdal.Open( fileRed, gdal.GA_ReadOnly )
        # Check the dataset was successfully opened
        if dataset is None:
            print("The dataset could not openned")
            sys.exit(-1)

        # Create the output dataset
        outDataset = self.createOutputImage(outFilePath, dataset)
        # Check the datasets was successfully created.
        if outDataset is None:
            print('Could not create output image')
            sys.exit(-1)

        #nbRasters = dataset.RasterCount
        #print("The dataset contains %s rasters" % (str(dataset.RasterCount)))
        red_band = gdal.Open(fileRed)
        print("The dataset contains %s rasters" % (str(red_band.RasterCount)))
        print("red_band is %i x %i" % (red_band.RasterXSize, red_band.RasterYSize))
        
        nir_band = gdal.Open(fileNir)
        print("The dataset contains %s rasters" % (str(nir_band.RasterCount)))
        print("nir_band is %i x %i" % (nir_band.RasterXSize, nir_band.RasterYSize))
        # Retrieve the number of lines within the image
        numLines = red_band.RasterYSize
        # Loop through each line in turn.
        for line in range(numLines):
            # Define variable for output line.
            outputLine = ''
            # Read in data for the current line from the 
            # image band representing the red wavelength
            red_scanline = red_band.ReadRaster( 0, line, red_band.RasterXSize, 1, \
                           red_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack the line of data to be read as floating point data
            red_tuple = struct.unpack('f' * red_band.RasterXSize, red_scanline)
            
            # Read in data for the current line from the 
            # image band representing the NIR wavelength
            nir_scanline = nir_band.ReadRaster( 0, line, nir_band.RasterXSize, 1, \
                           nir_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack the line of data to be read as floating point data
            nir_tuple = struct.unpack('f' * nir_band.RasterXSize, nir_scanline)

            # Loop through the columns within the image
            for i in range(len(red_tuple)):
                # Calculate the NDVI for the current pixel.
                ndvi_lower = (nir_tuple[i] + red_tuple[i])
                ndvi_upper = (nir_tuple[i] - red_tuple[i])
                ndvi = 0
                # Becareful of zero divide 
                if ndvi_lower == 0:
                    ndvi = 0
                else:
                    ndvi = ndvi_upper/ndvi_lower
                # Add the current pixel to the output line
                outputLine = outputLine + str(struct.pack('f', ndvi))
            # Write the completed line to the output image
            outDataset.GetRasterBand(1).WriteRaster(0, line, red_band.RasterXSize, 1, \
                                            outputLine, buf_xsize=red_band.RasterXSize, 
                                            buf_ysize=1, buf_type=gdal.GDT_Float32)
            print("Printed line %i on %i" % (line,numLines))
            # Delete the output line following write
            del outputLine
        print('NDVI Calculated and Outputted to File')
                
    # The function from which the script runs.
    def run(self,SAFEPath):
        # retrieve correct gfile path
        nirOK = False
        redOK = False
        
        for filename in glob.iglob(SAFEPath + '/**/*.jp2', recursive=True):
           if "B04.jp2" in filename:
               red_band_file = filename
               redOk = True
           if "B08.jp2" in filename:
               nir_band_file = filename
               nirOk = True
               
        if (nirOK == False or redOK == False):
            print("Some files are missing check your dataset")
        
        
        outFilePath = SAFEPath + "/S2_NVDI.tif"
        # Check the input file exists
        if os.path.exists(nir_band_file):
            # Run calcNDVI function
            self.calcNDVI(red_band_file,nir_band_file, outFilePath)
        else:
            print('The file does not exist.')

# Start the script by calling the run function.
if __name__ == '__main__':
    SAFE_product_path="/home/bruno/workspace/TutoDownload/prod/S2A_MSIL1C_20161126T051712_N0204_R033_T39DVC_20161126T051850.SAFE"
    obj = GDALCalcNDVI()
    obj.run(SAFE_product_path)
    