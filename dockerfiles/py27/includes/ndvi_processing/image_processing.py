import sys
import os
import struct
import glob
from osgeo import gdal


# Define the class GDALCalcNDVI
class GDALCalcNDVI(object):

    # A function to create the output image
    def createOutputImage(self, outFilename, inDataset):
        # Define the image driver to be used
        # This defines the output file format (e.g., GeoTiff)
        driver = gdal.GetDriverByName("GTiff")

        geoTransform = inDataset.GetGeoTransform()
        geoProjection = inDataset.GetProjection()
        # Create an output file of the same size as the inputted
        # image but with only 1 output image band.
        newDataset = driver.Create(outFilename, inDataset.RasterXSize,
                                   inDataset.RasterYSize, 1, gdal.GDT_Float32)
        # Define the spatial information for the new image.
        newDataset.SetGeoTransform(geoTransform)
        newDataset.SetProjection(geoProjection)
        return newDataset

    # The function which loops through the input image and
    # calculates the output NDVI value to be outputted.
    def calcNDVI(self, fileRed, fileNir, outFilePath):
        driver = gdal.GetDriverByName("")
        # Open the inputted dataset
        dataset = gdal.Open(fileRed, gdal.GA_ReadOnly)
        # Check the dataset was successfully opened
        if dataset is None:
            print("The dataset could not openned")
            sys.exit(-1)

        # Create the output dataset
        outDataset = self.createOutputImage(outFilePath, dataset)
        # Check the datasets was successfully created.
        if outDataset is None:
            print('Could not create output image')
            sys.exit(-1)

        red_band = gdal.Open(fileRed)
        print("The dataset contains %s rasters" % (str(red_band.RasterCount)))
        print("red_band is %i x %i" % (red_band.RasterXSize, red_band.RasterYSize))

        nir_band = gdal.Open(fileNir)
        print("The dataset contains %s rasters" % (str(nir_band.RasterCount)))
        print("nir_band is %i x %i" % (nir_band.RasterXSize, nir_band.RasterYSize))

        # Retrieve the number of lines within the image
        numLines = red_band.RasterYSize

        # Load 1000 lines at once
        num_lines_at_once = 1000

        current_line_offset = 0
        total_num_lines = red_band.RasterYSize
        total_num_columns = red_band.RasterXSize
        # Loop through each line in turn.
        while current_line_offset < total_num_lines:
            # During the last loop, read only the remaining lines
            if (total_num_lines - current_line_offset) < num_lines_at_once:
                num_lines_at_once = total_num_lines - current_line_offset

            red_lines = red_band.ReadRaster(0, current_line_offset, red_band.RasterXSize, num_lines_at_once,
                                                   red_band.RasterXSize, num_lines_at_once, gdal.GDT_Float32)
            red_tuple = struct.unpack('f' * red_band.RasterXSize * num_lines_at_once, red_lines)
            # del red_lines

            nir_lines = nir_band.ReadRaster(0, current_line_offset, nir_band.RasterXSize, num_lines_at_once,
                                                   nir_band.RasterXSize, num_lines_at_once, gdal.GDT_Float32)
            nir_tuple = struct.unpack('f' * nir_band.RasterXSize * num_lines_at_once, nir_lines)
            # del nir_lines

            temp_lines = bytearray(nir_band.RasterXSize * num_lines_at_once * 4 + 4)

            current_num_columns = 0
            current_num_line = current_line_offset
            # Loop through the columns within the image
            for i in range(len(red_tuple)):
                # Calculate the NDVI for the current pixel.
                ndvi_lower = (nir_tuple[i] + red_tuple[i])
                ndvi_upper = (nir_tuple[i] - red_tuple[i])
                ndvi = 0
                # Becareful of zero divide
                if ndvi_lower == 0:
                    ndvi = 0
                else:
                    ndvi = ndvi_upper / ndvi_lower
                # Add the current pixel to the output line
                struct.pack_into('f', temp_lines, 4 * i, ndvi)
                if current_num_columns >= total_num_columns:
                    current_num_line = current_num_line + 1
                    # Print current line, if multiple of 1000
                    if int((current_num_line + 1) / 1000) == ((current_num_line + 1) / 1000):
                        print("Printed line %i on %i" % ((current_num_line + 1), total_num_lines))
                    current_num_columns = 0
                else:
                    current_num_columns = current_num_columns + 1
            current_num_line = current_num_line + 1
            # Print current line, if multiple of 1000
            if int((current_num_line + 1) / 1000) == ((current_num_line + 1) / 1000):
                print("Printed line %i on %i" % ((current_num_line + 1), total_num_lines))

            # Write the completed line to the output image
            output_lines = bytes(temp_lines)
            outDataset.GetRasterBand(1).WriteRaster(0, current_line_offset, red_band.RasterXSize, num_lines_at_once,
                                                    output_lines, buf_xsize=red_band.RasterXSize,
                                                    buf_ysize=num_lines_at_once, buf_type=gdal.GDT_Float32)
            # Increment counter
            current_line_offset = current_line_offset + num_lines_at_once
            # Clean memory
            del output_lines
            del temp_lines
        print('NDVI Calculated and Outputted to File')

    # The function from which the script runs.
    def run(self, SAFEPath, output_directory):
        # retrieve correct gfile path
        nirOK = False
        redOK = False
        nir_band_file = None
        red_band_file = None

        for filename in glob.iglob(os.path.join(SAFEPath, '**', '*.jp2'), recursive=True):
            if "B04.jp2" in filename:
                red_band_file = filename
                redOK = True
            if "B08.jp2" in filename:
                nir_band_file = filename
                nirOK = True

        if (not nirOK) or (not redOK):
            print("Some files are missing check your dataset")

        outFilePath = os.path.join(output_directory, "S2_output_NVDI.tif")
        # Check the input file exists
        if os.path.exists(nir_band_file):
            # Run calcNDVI function
            self.calcNDVI(red_band_file, nir_band_file, outFilePath)
        else:
            print('The following file does not exist :')
            print(nir_band_file)
