# test_api.py

This python script shows how to use sobloo APIs to search and download data.
You are also introduced to several search features such as geonames.

Remember that you need a valid API KEY to be able to download data.
