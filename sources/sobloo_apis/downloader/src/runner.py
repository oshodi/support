#!/bin/python
# -*- coding: utf-8 -*-

import json
import os,sys
import traceback
from dwnld_light import Downloader


if __name__ == '__main__':

    pol = os.environ.get("POLYGON")
    sat = os.environ.get("SATELLITE")
    type = os.environ.get("TYPE_PROD")
    start = os.environ.get("SENSING_START")
    end = os.environ.get("SENSING_END")
    nbDwnld = os.environ.get("NB_DWNLD")
    output = os.environ.get("OUTPUT_PATH")
    apikey = os.environ.get("SB_APIKEY")

    dwnld = Downloader(apikey,nbDwnld,output)
    myresults = dwnld.getSafeList(polygon=pol, geoname=None, start_date=start, end_date=end, type=type, sat=sat, orbit_dir=None, source=None)
    dwnld.downloadSafeList(myresults,output)

    """
    pol = "POLYGON((-8.278040784499353 10.493595909047388,-2.3454235969993533 10.493595909047388,-2.3454235969993533 4.220809563194211,-8.278040784499353 4.220809563194211))"
    sat = "S2A"
    type = "S2MSI1C"
    start = "20-03-2019"
    end = "30-03-2019"
    """
