#!/bin/python
# -*- coding: utf-8 -*-
import numbers
import datetime
import json
import os
from py_logger import PyLogger
import traceback
import requests
import shutil

class Downloader():

    def __init__(self, apikey, nbdownloads, outputPath):
      """
      Definition de la cle API
      """
      self.__apikey = apikey
      self.__nbdwnld = int(nbdownloads)
      self.mylog = PyLogger("INFO","dwnld_light",outputPath)

    def getSafeList(self, polygon=None, geoname=None, start_date=None, end_date=None, type=None, sat=None, orbit_dir=None, source=None):

        """
        Recuperation de la liste des produit correspondant aux parametres d'entrée

        Args:
          polygon (str) : Polygone de type WKT sur la  zone à couvrir
          geoname (str) : Nom du pays à couvrir
          start_date (str) : date de début d'acquisition (format : DD-MM-YYYY)
          end_date (str) : date de fin d'acquisition
          type (str) : Type de données
          sat (str) : identifiant du satellite (S1A, S1B, S2A, S2B)
          orbit_dir (str) : orbite (ascendant ou descendant)

        Returns:
          result (list) : list de dictionnaires des produits correspondant.

        """
        result = []

        base_url="https://sobloo.eu/api/v1/services/search?"

        params = []

        if start_date != None and end_date != None :
            timeStart = int(datetime.datetime.strptime(start_date, '%d-%m-%Y').strftime("%s")) * 1000
            timeEnd = int(datetime.datetime.strptime(end_date, '%d-%m-%Y').strftime("%s")) * 1000
            params.append( ('f', 'timeStamp', 'range', '[' + str(timeStart) + '<' + str(timeEnd) + ']') )
        elif start_date != None :
            timeStart = int(datetime.datetime.strptime(start_date, '%d-%m-%Y').strftime("%s")) *1000
            params.append( ('f', 'timeStamp', 'gte', str(timeStart) ) )
        elif end_date != None :
            timeEnd = int(datetime.datetime.strptime(end_date, '%d-%m-%Y').strftime("%s")) *1000
            params.append( ('f', 'timeStamp', 'lte', str(timeEnd) ) )
        if type != None :
          params.append( ('f', 'identification.type', 'eq', type) )
        if sat != None :
          params.append( ('f', 'acquisition.missionCode', 'eq', sat) )
        if orbit_dir != None :
          params.append( ('f', 'orbit.direction', 'eq', orbit_dir) )
        if geoname != None :
          params.append( ('f', 'enrichment.geonames.name', 'eq', geoname) )
        if source != None :
          params.append( ('f', 'state.services.download', 'eq', source) )

        nb_hits_parcourus = 0
        totalnb = -1

        cpt_error = 0

        while nb_hits_parcourus != totalnb:
            if cpt_error == 3 :
                return None
            params_string=""
            if polygon != None:
                params_string = "gintersect=%s&" % polygon
            for param in params:
                params_string +=  str(param[0]) + "=" + str(param[1]) + ":" + str(param[2]) + ":" + str(param[3])
                params_string += "&"
                url = base_url + params_string + "from=" + str(nb_hits_parcourus) + "&include=uid,identification&size=1000"

            try:
                self.mylog.info("URL=%s" % (url))
                r = requests.get(url)

                if r.status_code == 200:
                    result_request = r.json()
                    totalnb = int(result_request['totalnb'])
                    nb_hits = int(result_request['nbhits'])
                    self.mylog.info("Total nb: %s  NbHits: %s" % (str(totalnb),str(nb_hits)))

                    if nb_hits > 0 :
                        for hit in result_request['hits'] :
                            result.append(
                            {
                            'uid': hit['data']['uid'],
                            'id': hit['data']['identification']['externalId']
                            })

                            if len(result) >= self.__nbdwnld:
                                return result

                        nb_hits_parcourus += nb_hits
                else:
                    self.mylog.error("Search APIs returned error %s at %s hits read" % (r.status_code,nb_hits_parcourus))
                    return result

            except Exception as e:
                traceback.print_exc()
                self.mylog.error("Error during request URL=%s : %s" % (url, str(e)))
                cpt_error += 1
                return None

        return result

    def downloadSafeList(self, safeList, folder_dest):
        """
        Téléchargement des produits
        Args:
            safeList (dict) : Dictionnaire du fichier à télécharger {uid, id}
            folder_dest (str) : répertoire de destination des fichiers téléchargés
        Returns :
          files (list) : liste des fichiers .SAFE
        """

        headers = {'authorization': 'Apikey ' + self.__apikey}
        base_url="https://sobloo.eu/api/v1/services/download/"

        files = []
        try:
            for safe in safeList:
                filename = os.path.join(folder_dest, safe['id'] + ".zip")
                url = base_url + safe['uid']

                dwnldStart = datetime.datetime.now()
                self.mylog.error("Product uid: %s xid: %s start download at %s" % (safe['uid'], safe['id'], str(dwnldStart)))
                r = requests.get(url, stream=True, headers=headers, timeout=None)

                if r.status_code == 200:
                    with open(filename, 'wb') as f:
                        r.raw.decode_content = True
                        shutil.copyfileobj(r.raw, f)
                        files.append(filename)

                    dwnldStop = datetime.datetime.now()
                    deltaTime = (dwnldStop - dwnldStart).total_seconds()
                    self.mylog.info("Product uid: %s xid: %s downloaded in %s seconds" % (safe['uid'], safe['id'],str(deltaTime)))
                else:
                    dwnldError = datetime.datetime.now()
                    self.mylog.error("Product uid: %s xid: %s not downloaded at %s with error: %s" % (safe['uid'], safe['id']),str(dwnldError), r.status_code)
            return files
        except:
            traceback.print_exc()
            self.mylog.error("Error during request URL=%s" % (url))
            return None
