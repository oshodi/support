GDAL processing
==
This python script uses GDAL libs to process data.
In this example, a Sentinel-2 L1C product is downloaded
and processed to generate a NDVI map.
