# This file shows the answers to provide when configuring s3cmd
# with s3cmd --configure

AccessKey:
SecretKey:
region: eu-west-0
S3-Endpoint: oss.prod-cloud-ocb.orange-business.com
DNS-style: oss.prod-cloud-ocb.orange-business.com
HTTPS: yes or no
Proxy: following your connection

