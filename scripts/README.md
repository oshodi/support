The scripts provided here allow interacting with your sobloo environment.  

Orange cloud
==

VM Start/Stop/List
--
The script 'srvmgmt.sh' allows starting, stopping, listing the VMs located ina particular tenant.  
You need a valid account in the Orange cloud with an API password (refer to Orange doc to generate it) 
The commands supported are:  
 - START: to start a VM
 - STOP: to stop a VM
 - STAT: to list all the VMs in the tenant. In that case the server name does not matter but a string must be provided.

Data access
==

S3 storage access
--
You can use the tool s3cmd to access your buckets. Read the file 'config-s3cmd.txt' to configure it.


Miscellaneous
==

Docker installation on Ubuntu
--
Run the script 'install_docker_ubuntu.sh' on your VM to get docker installed in a minute.

