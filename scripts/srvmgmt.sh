if [ $# -ne 5 ] ; then
  echo "This script allows to start/stop/list VMs in a tenant"
  echo "You must provide parameters: START/STOP/STAT TenantID(=OCB00XXXX) CloudUser UserAPIpaswword VMname"
else
  cmd=$1
  tenant=$2
  user=$3
  passwd=$4
  srv=$5
  docker run --rm registry.gitlab.com/pub-sobloo/support/svrmgmt:latest $cmd $tenant $user $passwd $srv
fi
