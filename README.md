# support
Welcome to sobloo environment. In order to get you in as fast as possible, you will find here resources covering many sobloo aspects.  
The git is organised as follows:
 - A repository where you can find dockerfiles, source code and scripts
 - A docker registry providing several docker images ready to use (python development, OpenStack oriented, etc.)
 - A wiki to share documentation with you


Enjoy !

